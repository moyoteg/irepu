//
//  APIManager.m
//  iRepU
//
//  Created by Jaime Moises Gutierrez on 3/30/15.
//  Copyright (c) 2015 JMGL. All rights reserved.
//

#import "APIManager.h"

@implementation APIManager

/*
 Who represents You in the U.S. Congress
 API
 
 If you would like to use this data in your application, I offer an API to retrieve it. In it's current state, this involves calling a PHP script directly with the zip information and XML is returned with the congressperson's info. You can optionally have the data returned in JSON by calling the API using "&output=json".
 */

/*
 getall_mems.php
 
 DESCRIPTION: returns data on both representatives and senators by zipcode
 INPUT: zip, output (optional)
 RETURNS: name, state, district, phone, office, website
 EXAMPLE: http://whoismyrepresentative.com/getall_mems.php?zip=31023
 */
+ (NSDictionary *)getAllMembersByZipcode:(NSString *)zipCode
{
    return @{};
}

/*
 getall_reps_byname.php
 
 DESCRIPTION: returns data on representatives by lastname
 INPUT: lastname, output (optional)
 RETURNS: name, state, district, phone, office, website
 EXAMPLE: http://whoismyrepresentative.com/getall_reps_byname.php?name=smith
 */

+ (NSDictionary *)getAllRepresentativesByLastName:(NSString *)lastName
{
    return @{};
}
/*
 getall_reps_bystate.php
 
 DESCRIPTION: returns data on representatives by state
 INPUT: state, output (optional)
 RETURNS: name, state, district, phone, office, website
 EXAMPLE: http://whoismyrepresentative.com/getall_reps_bystate.php?state=CA
 */

+ (NSDictionary *)getAllRepresentativesByState:(NSString *)state
{
    return @{};
}
/*
 getall_sens_byname.php
 
 DESCRIPTION: returns data on senators by lastname
 INPUT: lastname, output (optional)
 RETURNS: name, state, district, phone, office, website
 EXAMPLE: http://whoismyrepresentative.com/getall_sens_byname.php?name=johnson
 */

+ (NSDictionary *)getAllSenatorsByName:(NSString *)name
{
    return @{};
}

/*
 getall_sens_bystate.php
 
 DESCRIPTION: returns data on senators by state
 INPUT: state, output (optional)
 RETURNS: name, state, district, phone, office, website
 EXAMPLE: http://whoismyrepresentative.com/getall_sens_bystate.php?state=ME
 */

+ (NSDictionary *)getAllSEnatorsByState:(NSString *)state
{
    return @{};
}

/*
 */

@end
