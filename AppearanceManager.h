//
//  AppearanceManager.h
//  CelebApp
//
//  Created by Jaime Moises Gutierrez on 1/22/15.
//
//

#import <Foundation/Foundation.h>

@interface AppearanceManager : NSObject

- (void)setAppearanceCurrentConfiguration;

+ (UIColor*)colorWithHexString:(NSString*)hex;

+ (void)addVibrancyAndBlurr:(UIView *)view;

+ (void)addGradientToView:(UIView *)view;

+ (void)addShadowToView:(UIView *)view;

+ (UIImage *)imageWithRoundedCornersSize:(float)cornerRadius usingImage:(UIImage *)original imageView:(UIImageView *)imageView;

+(void)makeRoundedView:(UIView *)view;

+ (void)roundedLayer:(CALayer *)viewLayer;

+ (void)drawAngledGradientInView:(UIView *)view atAngle:(float)angle;

@end
