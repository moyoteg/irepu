//
//  Color_Constants.h
//  CelebApp
//
//  Created by Jaime Moises Gutierrez on 1/23/15.
//
// This file is meant to help with theme color utilization.
//

#ifndef CelebApp_Color_Constants_h
#define CelebApp_Color_Constants_h


#endif

//self.view.backgroundColor = UIColorFromRGB(0xCECECE);
//RGB color macro
#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//self.view.backgroundColor = UIColorFromRGBWithAlpha(0xCECECE, 0.8);
//RGB color macro with alpha
#define UIColorFromRGBWithAlpha(rgbValue,a) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]

//Gradient Colors

//SIngle Colors

#define KThemeGradient1from [UIColor colorWithRed:1.000000F green:0.368627F blue:0.227451F alpha:1.0F]
#define KThemeGradient1to [UIColor colorWithRed:1.000000F green:0.164706F blue:0.407843F alpha:1.0F]