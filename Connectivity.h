//
//  Connectivity.h
//  CelebApp
//
//  Created by Jaime Moises Gutierrez on 9/14/14.
//
//

#import <Foundation/Foundation.h>

@interface Connectivity : NSObject

+ (BOOL)connected;
+ (BOOL)internetConnectionIsActive;

@end
