//
//  Connectivity.m
//  CelebApp
//
//  Created by Jaime Moises Gutierrez on 9/14/14.
//
//

#import "Connectivity.h"
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "NotificationManager.h"

@implementation Connectivity

+ (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    switch (networkStatus)
    {
        case NotReachable:
            NSLog(@"Network status NotReachable");

            return false;

            break;
        case ReachableViaWiFi:
            NSLog(@"Network status ReachableViaWiFi");

            return true;

            break;
        case ReachableViaWWAN:
            NSLog(@"Network status ReachableViaWWAN");

            return true;

            break;
            
        default:
            break;
    }
}

+ (BOOL)internetConnectionIsActive
{
    if ([Connectivity connected])
    {
        return true;
    }
    else
    {
        [NotificationManager alertWithMessage:@"No Internet Connection"];
        return false;
    }    
}

@end
