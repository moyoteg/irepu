//
//  CurrentBuildConfigurationController.h
//  CelebApp
//
//  Created by Jaime Moises Gutierrez on 10/29/14.
//
//

#import <Foundation/Foundation.h>

#define kTestingOrDevelopment NO

// Celeb Data
#define kCelebrityDataDownloadedBoolKey @"kCelebrityDataDownloadedBoolKey"
#define kMustUpdateCelebrityData @"kMustUpdateCelebrityData"

@interface CurrentBuildConfigurationController : NSObject

+ (NSDictionary *)getCurrentBuildConfiguration;

// Admob
+ (NSString *)getkAdmobPublisherID;
+ (NSString *)getkAdmobAdID;

// Parse
+ (NSString *)getkParseApplicationID;
+ (NSString *)getkParseClientKey;

// Celebrity
+ (NSString *)getkCelebrityName;
+ (NSString *)getkProfilePicURL;
+ (NSString *)getkTwitterURL;
+ (NSString *)getkFacebookURL;
+ (NSString *)getkVineURL;
+ (NSString *)getkInstagramURL;
+ (NSString *)getkYoutubeURL;

+ (NSString *)getkContentUpdateURL;

+ (NSArray *)getkFeedURLDictionaryArray;

// Flurry
+ (NSString *)getkFlurryAPIKey;

// Plist
+ (NSString *)getDataPlistName;

// Color Theme
+ (NSDictionary *)getColorTheme;

@end
