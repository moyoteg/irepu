//
//  CurrentBuildConfigurationController.m
//  CelebApp
//
//  Created by Jaime Moises Gutierrez on 10/29/14.
//
//

#import "CurrentBuildConfigurationController.h"

// Admob
#define kAdmobAdID @"AdmobAdID"
#define kAdmobPublisherID @"AdmobPublisherID"

// Parse
#define kParseApplicationID @"ParseApplicationID"
#define kParseClientKey @"ParseClientKey"

//Celebrity
#define kCelebrityName @"Celebrity Name"
#define kTwitterURL @"Twitter URL feed"
#define kFacebookURL @"Facebook URL feed"
#define kVineURL @"Vine URL feed"
#define kInstagramURL @"Instagram URL feed"
#define kYoutubeURL @"Youtube URL feed"
#define kProfilePicURL @"Profile Pic URL"
#define kContentUpdateURL @"Content Update URL"

// Flurry
#define kFlurryAPIKey @"FlurryAPIKey"

// Color Theme
#define kColorThemeData @"ColorThemeData"

@implementation CurrentBuildConfigurationController

+ (NSString *)getDataPlistName
{
    return [NSString stringWithFormat:@"Celeb_data"];
}

+ (NSDictionary *)getCurrentBuildConfiguration
{
    NSString* configuration = [[[NSBundle mainBundle] infoDictionary] objectForKey:kCelebrityName];
//    NSLog(@"configuration file: \n%@",configuration);
    NSString* envsPListPath;
    
    if ([CurrentBuildConfigurationController celebDataPlistExistsInDocumentsFolderAlready])
    {
        envsPListPath = [CurrentBuildConfigurationController getCelebDataDocumentsFolderPlistFilePath];
    }
    else
    {
        NSBundle* bundle = [NSBundle mainBundle];
        envsPListPath = [bundle pathForResource:[CurrentBuildConfigurationController getDataPlistName] ofType:@"plist"];
    }
    
    NSDictionary* environments = [[NSDictionary alloc] initWithContentsOfFile:envsPListPath];
    NSDictionary* environment = [environments objectForKey:configuration];
    return environment;
}

// Admob
+ (NSString *)getkAdmobPublisherID
{
    NSLog(@"kAdmobPublisherID: %@",[[[NSBundle mainBundle] infoDictionary] objectForKey:kAdmobPublisherID]);
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:kAdmobPublisherID];
}

+ (NSString *)getkAdmobAdID
{
    NSLog(@"kAdmobAdID: %@",[[[NSBundle mainBundle] infoDictionary] objectForKey:kAdmobAdID]);
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:kAdmobAdID];
}

// Parse
+ (NSString *)getkParseApplicationID
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:kParseApplicationID];
}

+ (NSString *)getkParseClientKey
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:kParseClientKey];
}

// Celebrity
+ (NSString *)getkCelebrityName
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:kCelebrityName];
}

+ (NSString *)getkProfilePicURL
{
    return [[CurrentBuildConfigurationController getCurrentBuildConfiguration] objectForKey:kProfilePicURL];
}

+ (NSString *)getkTwitterURL
{
    return [[CurrentBuildConfigurationController getCurrentBuildConfiguration] objectForKey:[self getKeyForFeedURL:kTwitterURL]];
}

+ (NSString *)getkFacebookURL
{
    return [[CurrentBuildConfigurationController getCurrentBuildConfiguration] objectForKey:[self getKeyForFeedURL:kFacebookURL]];
}

+ (NSString *)getkVineURL
{
    return [[CurrentBuildConfigurationController getCurrentBuildConfiguration] objectForKey:[self getKeyForFeedURL:kVineURL]];
}

+ (NSString *)getkInstagramURL
{
    return [[CurrentBuildConfigurationController getCurrentBuildConfiguration] objectForKey:[self getKeyForFeedURL:kInstagramURL]];
}

+ (NSString *)getkYoutubeURL
{
    return [[CurrentBuildConfigurationController getCurrentBuildConfiguration] objectForKey:[self getKeyForFeedURL:kYoutubeURL]];
}

+ (NSString *)getKeyForFeedURL:(NSString *)keyWord
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self CONTAINS[cd] %@", keyWord];

    return [[[[CurrentBuildConfigurationController getCurrentBuildConfiguration] allKeys] filteredArrayUsingPredicate:predicate] objectAtIndex:0];
}

+ (NSString *)getkContentUpdateURL
{
    return [[CurrentBuildConfigurationController getCurrentBuildConfiguration] objectForKey:[self getKeyForFeedURL:kContentUpdateURL]];
}

+ (NSArray *)getkFeedURLDictionaryArray
{
    NSString *searchTextURL = @"URL";
    NSString *searchTexFeed = @"feed";
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self CONTAINS[cd] %@ AND self CONTAINS[cd] %@", searchTextURL, searchTexFeed];
    
    NSMutableArray *filteredKeys = [NSMutableArray arrayWithArray:[[[CurrentBuildConfigurationController getCurrentBuildConfiguration] allKeys] filteredArrayUsingPredicate:predicate]];
    filteredKeys = [NSMutableArray arrayWithArray:[filteredKeys sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)]];
    
    NSMutableArray *URLDictArray = [[NSMutableArray alloc] init];
    for (int i = 0; [filteredKeys count] > i ; i++)
    {
        NSMutableDictionary *urlDict = [[NSMutableDictionary alloc] init];

        [urlDict setValue:[[CurrentBuildConfigurationController getCurrentBuildConfiguration] objectForKey:[filteredKeys objectAtIndex:i]] forKey:[self removeIndex:[filteredKeys objectAtIndex:i]]];
        [URLDictArray addObject:urlDict];
    }
    return [URLDictArray copy];
}

+ (NSString *)removeIndex:(NSString *)str
{
    NSMutableString *string = [str mutableCopy];
    
    NSRegularExpression *regex = [NSRegularExpression
                                  regularExpressionWithPattern:@"\\(.+?\\)"
                                  options:NSRegularExpressionCaseInsensitive
                                  error:NULL];
    
    [regex replaceMatchesInString:string
                          options:0
                            range:NSMakeRange(0, [str length])
                     withTemplate:@""];
    return [string copy];
}

+ (NSString *)getkFlurryAPIKey
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:kFlurryAPIKey];
}

// Theme

+ (NSDictionary *)getColorTheme
{
    return [[CurrentBuildConfigurationController getCurrentBuildConfiguration] objectForKey:kColorThemeData];
}

#pragma mark - PList Methods

+ (NSString *)getCelebDataDocumentsFolderPlistFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [NSString stringWithFormat:@"%@/%@.plist", documentsDirectory, [CurrentBuildConfigurationController getDataPlistName]];
}

+ (BOOL)celebDataPlistExistsInDocumentsFolderAlready
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@.plist", documentsDirectory, [CurrentBuildConfigurationController getDataPlistName]];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:filePath])
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

+ (void)createCelebDataPlist
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [NSString stringWithFormat:@"%@/%@.plist", documentsDirectory, [CurrentBuildConfigurationController getDataPlistName]];
    
    NSBundle* bundle = [NSBundle mainBundle];
    NSString* envsPListPath = [bundle pathForResource:[CurrentBuildConfigurationController getDataPlistName] ofType:@"plist"];
    NSDictionary* environments = [[NSDictionary alloc] initWithContentsOfFile:envsPListPath];
    [environments writeToFile:filePath atomically:YES];
}

@end
