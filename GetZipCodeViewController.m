//
//  GetZipCodeViewController.m
//  iRepU
//
//  Created by Jaime Moises Gutierrez on 3/30/15.
//  Copyright (c) 2015 JMGL. All rights reserved.
//

#import "GetZipCodeViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <MapKit/MapKit.h>
#import "NotificationManager.h"

@interface GetZipCodeViewController () <MKMapViewDelegate, CLLocationManagerDelegate>
@property (strong, nonatomic) IBOutlet UITextField *zipCodeTexField;

@end

@implementation GetZipCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //---- For getting current gps location
    
    [self.zipCodeTexField becomeFirstResponder];
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
//{
//    currentLocation = [locations objectAtIndex:0];
//    [locationManager stopUpdatingLocation];
//    
//    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
//    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error)
//     {
//         if (!(error))
//         {
//             CLPlacemark *placemark = [placemarks objectAtIndex:0];
//             NSLog(@"\nCurrent Location Detected\n");
//             NSLog(@"placemark %@",placemark);
//             NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
//             
//             NSString *Address = [[NSString alloc]initWithString:locatedAt];
//             NSString *Zipcode = [[NSString alloc]initWithString:placemark.postalCode];
//             NSLog(@"%@",Zipcode);
//         }
//         else
//         {
//             NSLog(@"Geocode failed with error %@", error); // Error handling must required
//         }
//     }];
//}

- (IBAction)pressedCancelButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (IBAction)pressedDoneButton:(id)sender {
    
    if (self.zipCodeTexField.text.length == 5)
    {
        [[NSNotificationCenter defaultCenter]
         postNotificationName:@"SearchByZip"
         object:nil userInfo:@{@"zipCode":self.zipCodeTexField.text}];
        
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }
    else
    {
        [NotificationManager alertWithMessage:@"Not a valid Zip Code!"];
    }
}

@end
