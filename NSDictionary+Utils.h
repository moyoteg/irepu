//
//  NSDictionary+Utils.h
//  CelebApp
//
//  Created by Jaime Moises Gutierrez on 2/13/15.
//
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Utils)

-(BOOL)hasValueForKey:(NSString *)key;
-(id)JSONRepresentation;

@end
