//
//  NSDictionary+Utils.m
//  CelebApp
//
//  Created by Jaime Moises Gutierrez on 2/13/15.
//
//

#import "NSDictionary+Utils.h"

@implementation NSDictionary (Utils)

-(BOOL)hasValueForKey:(NSString *)key
{
    if([self objectForKey:key] == nil){
        return false;
    }
    
    if([[self objectForKey:key] isKindOfClass:[NSNull class]]){
        return false;
    }
    
    return true;
    
}

-(id)JSONRepresentation
{
    NSError *error;
    NSData *data = [NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:&error];
    
        //NSString *string = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        //NSLog(@"Json value for Sample: %@", string);
    
    return data;
}

@end
