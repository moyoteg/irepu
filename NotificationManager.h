//
//  NotificationManager.h
//  CelebApp
//
//  Created by Jaime Moises Gutierrez on 2/10/15.
//
//

#import <Foundation/Foundation.h>

@interface NotificationManager : NSObject

// User Points
+ (void)userGamificationPointsChanged;
+ (void)userGamificationPointsChanged:(NSInteger)points;

// Youtube View Controller
+ (void)alertWithMessage:(NSString *)message;
+ (void)completionWithMessage:(NSString *)message;
+ (void)fullScreenAlertWIthTitle:(NSString *)title message:(NSString *)message withCancelButton:(BOOL)cancelButton;

@end
