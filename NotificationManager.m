//
//  NotificationManager.m
//  CelebApp
//
//  Created by Jaime Moises Gutierrez on 2/10/15.
//
//

#import "NotificationManager.h"
#import "CRToast.h"
#import "AMSmoothAlertView.h"

@implementation NotificationManager

#define kNotificationDuration 0.6

#pragma mark - CRToast
#pragma mark - Gamification Notifications
+ (void)userGamificationPointsChanged
{
    NSMutableDictionary *options = [@{
                              kCRToastTextKey : @"Gamification Notification!",
                              kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                              kCRToastBackgroundColorKey : [UIColor orangeColor],
                              kCRToastAnimationInTypeKey : @(CRToastAnimationTypeSpring),
                              kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                              kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionLeft),
                              kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionRight),
                              kCRToastTimeIntervalKey : @(kNotificationDuration),
                              kCRToastImageKey : [UIImage imageNamed:@"white_checkmark"],
                              } mutableCopy];
    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"Completed");
                                }];
}

+ (void)userGamificationPointsChanged:(NSInteger)points
{
    NSMutableDictionary *options = [@{
                                      kCRToastTextKey : [NSString stringWithFormat:@"Earned points:%ld!", (long)points],
                                      kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                                      kCRToastBackgroundColorKey : [UIColor orangeColor],
                                      kCRToastAnimationInTypeKey : @(CRToastAnimationTypeSpring),
                                      kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                                      kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionBottom),
                                      kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                                      kCRToastTimeIntervalKey : @(kNotificationDuration),
                                      kCRToastImageKey : [UIImage imageNamed:@"white_checkmark"],
                                      } mutableCopy];
    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"Completed");
                                }];
}

#pragma mark - CRToast Standard Notifications

+ (void)alertWithMessage:(NSString *)message
{
    NSMutableDictionary *options = [@{
                                      kCRToastUnderStatusBarKey : @YES,
                                      kCRToastFontKey : [UIFont systemFontOfSize:22.0],
                                      kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                                      kCRToastTextKey : [NSString stringWithFormat:@"%@",message],
                                      kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                                      kCRToastBackgroundColorKey : [UIColor redColor],
                                      kCRToastAnimationInTypeKey : @(CRToastAnimationTypeSpring),
                                      kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                                      kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionBottom),
                                      kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                                      kCRToastTimeIntervalKey : @(kNotificationDuration),
                                      kCRToastImageKey : [UIImage imageNamed:@"alert_icon"],
                                      } mutableCopy];
        
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"Completed");
                                }];
}

+ (void)completionWithMessage:(NSString *)message
{
    NSMutableDictionary *options = [@{
                                      kCRToastFontKey : [UIFont systemFontOfSize:22.0],
                                      kCRToastNotificationTypeKey : @(CRToastTypeNavigationBar),
                                      kCRToastTextKey : [NSString stringWithFormat:@"%@",message],
                                      kCRToastTextAlignmentKey : @(NSTextAlignmentCenter),
                                      kCRToastBackgroundColorKey : [UIColor purpleColor],
                                      kCRToastAnimationInTypeKey : @(CRToastAnimationTypeSpring),
                                      kCRToastAnimationOutTypeKey : @(CRToastAnimationTypeGravity),
                                      kCRToastAnimationInDirectionKey : @(CRToastAnimationDirectionBottom),
                                      kCRToastAnimationOutDirectionKey : @(CRToastAnimationDirectionTop),
                                      kCRToastTimeIntervalKey : @(kNotificationDuration),
                                      kCRToastImageKey : [UIImage imageNamed:@"teal_checkmark"],
                                      } mutableCopy];
    
    [CRToastManager showNotificationWithOptions:options
                                completionBlock:^{
                                    NSLog(@"Completed");
                                }];
}

#pragma mark - AMSmoothAlertView

#pragma mark - CRToast Standard Notifications

+ (void)fullScreenAlertWIthTitle:(NSString *)title message:(NSString *)message withCancelButton:(BOOL)cancelButton
{
	AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]initDropAlertWithTitle:title andText:message andCancelButton:cancelButton forAlertType:AlertFailure];
	[alert show];
}

@end
