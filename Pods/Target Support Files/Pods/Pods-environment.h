
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AMSmoothAlert
#define COCOAPODS_POD_AVAILABLE_AMSmoothAlert
#define COCOAPODS_VERSION_MAJOR_AMSmoothAlert 1
#define COCOAPODS_VERSION_MINOR_AMSmoothAlert 1
#define COCOAPODS_VERSION_PATCH_AMSmoothAlert 0

// CRToast
#define COCOAPODS_POD_AVAILABLE_CRToast
#define COCOAPODS_VERSION_MAJOR_CRToast 0
#define COCOAPODS_VERSION_MINOR_CRToast 0
#define COCOAPODS_VERSION_PATCH_CRToast 7

// FXBlurView
#define COCOAPODS_POD_AVAILABLE_FXBlurView
#define COCOAPODS_VERSION_MAJOR_FXBlurView 1
#define COCOAPODS_VERSION_MINOR_FXBlurView 6
#define COCOAPODS_VERSION_PATCH_FXBlurView 3

// GPUImage
#define COCOAPODS_POD_AVAILABLE_GPUImage
#define COCOAPODS_VERSION_MAJOR_GPUImage 0
#define COCOAPODS_VERSION_MINOR_GPUImage 1
#define COCOAPODS_VERSION_PATCH_GPUImage 6

// JAMSVGImage
#define COCOAPODS_POD_AVAILABLE_JAMSVGImage
#define COCOAPODS_VERSION_MAJOR_JAMSVGImage 1
#define COCOAPODS_VERSION_MINOR_JAMSVGImage 6
#define COCOAPODS_VERSION_PATCH_JAMSVGImage 0

// MBProgressHUD
#define COCOAPODS_POD_AVAILABLE_MBProgressHUD
#define COCOAPODS_VERSION_MAJOR_MBProgressHUD 0
#define COCOAPODS_VERSION_MINOR_MBProgressHUD 9
#define COCOAPODS_VERSION_PATCH_MBProgressHUD 1

// Reachability
#define COCOAPODS_POD_AVAILABLE_Reachability
#define COCOAPODS_VERSION_MAJOR_Reachability 3
#define COCOAPODS_VERSION_MINOR_Reachability 1
#define COCOAPODS_VERSION_PATCH_Reachability 1

// SDWebImage
#define COCOAPODS_POD_AVAILABLE_SDWebImage
#define COCOAPODS_VERSION_MAJOR_SDWebImage 3
#define COCOAPODS_VERSION_MINOR_SDWebImage 7
#define COCOAPODS_VERSION_PATCH_SDWebImage 2

// SDWebImage/Core
#define COCOAPODS_POD_AVAILABLE_SDWebImage_Core
#define COCOAPODS_VERSION_MAJOR_SDWebImage_Core 3
#define COCOAPODS_VERSION_MINOR_SDWebImage_Core 7
#define COCOAPODS_VERSION_PATCH_SDWebImage_Core 2

