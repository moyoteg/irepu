# README #
#What is this?#
I made this App for interviews, the project is not finished but it shows knowledge a lot of important concepts of iOS technologies.

##Quick summary##
* Version: 0.1
* Auto Layout
* Supports any device size
* Use of Cocoapods
* Use of CoreData
* Use of Interface Builder
* Size classes
* Memory management
* etc...

### How do I get set up? ###

* Just fork and play!

### Contribution guidelines ###

* Writing tests
* Code review
* Let me know what you want to add! @moyoteg

### Who do I talk to? ###

* moyoteg@gmail.com