//
//  Representative.m
//  iRepU
//
//  Created by Jaime Moises Gutierrez on 3/31/15.
//  Copyright (c) 2015 JMGL. All rights reserved.
//

#import "Representative.h"
#import "Search.h"


@implementation Representative

@dynamic name;
@dynamic state;
@dynamic party;
@dynamic phone;
@dynamic district;
@dynamic office;
@dynamic link;
@dynamic search;

@end
