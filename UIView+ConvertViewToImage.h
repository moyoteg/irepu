//
//  UIView+ConvertViewToImage.h
//  CelebApp
//
//  Created by Jaime Moises Gutierrez on 2/23/15.
//
//

#import <UIKit/UIKit.h>

@interface UIView (ConvertViewToImage)

-(UIImage *)convertViewToImage;

@end
