//
//  UIView+ConvertViewToImage.m
//  CelebApp
//
//  Created by Jaime Moises Gutierrez on 2/23/15.
//
//

#import "UIView+ConvertViewToImage.h"

@implementation UIView (ConvertViewToImage)

-(UIImage *)convertViewToImage
{
    UIGraphicsBeginImageContext(self.bounds.size);
    [self drawViewHierarchyInRect:self.bounds afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
