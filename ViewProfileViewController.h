//
//  ViewProfileViewController.h
//  iRepU
//
//  Created by Jaime Moises Gutierrez on 3/30/15.
//  Copyright (c) 2015 JMGL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewProfileViewController : UIViewController

@property (nonatomic, strong) NSDictionary *profileDict;

@end
