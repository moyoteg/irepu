//
//  ViewProfileViewController.m
//  iRepU
//
//  Created by Jaime Moises Gutierrez on 3/30/15.
//  Copyright (c) 2015 JMGL. All rights reserved.
//

#import "ViewProfileViewController.h"

@interface ViewProfileViewController ()
@property (strong, nonatomic) IBOutlet UIImageView *profileUIimageView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *partyLabel;
@property (strong, nonatomic) IBOutlet UILabel *stateLabel;
@property (strong, nonatomic) IBOutlet UILabel *districtLabel;
@property (strong, nonatomic) IBOutlet UILabel *phoneLabel;
@property (strong, nonatomic) IBOutlet UILabel *officeLabel;
@property (strong, nonatomic) IBOutlet UILabel *linkLabel;

@end

@implementation ViewProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.nameLabel.text = [self.profileDict objectForKey:@"name"];
    self.partyLabel.text = [self.profileDict objectForKey:@"party"];
    self.stateLabel.text = [self.profileDict objectForKey:@"state"];
    self.districtLabel.text = [self.profileDict objectForKey:@"district"];
    self.phoneLabel.text = [self.profileDict objectForKey:@"phone"];
    self.officeLabel.text = [self.profileDict objectForKey:@"office"];
    self.linkLabel.text = [self.profileDict objectForKey:@"link"];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
