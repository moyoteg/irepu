//
//  Search.h
//  iRepU
//
//  Created by Jaime Moises Gutierrez on 3/31/15.
//  Copyright (c) 2015 JMGL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Representative;

@interface Search : NSManagedObject

@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSSet *representatives;
@end

@interface Search (CoreDataGeneratedAccessors)

- (void)addRepresentativesObject:(Representative *)value;
- (void)removeRepresentativesObject:(Representative *)value;
- (void)addRepresentatives:(NSSet *)values;
- (void)removeRepresentatives:(NSSet *)values;

@end
