//
//  Search.m
//  iRepU
//
//  Created by Jaime Moises Gutierrez on 3/31/15.
//  Copyright (c) 2015 JMGL. All rights reserved.
//

#import "Search.h"
#import "Representative.h"


@implementation Search

@dynamic type;
@dynamic date;
@dynamic representatives;

@end
