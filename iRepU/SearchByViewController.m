//
//  SearchByViewController.m
//  iRepU
//
//  Created by Jaime Moises Gutierrez on 3/30/15.
//  Copyright (c) 2015 JMGL. All rights reserved.
//

#import "SearchByViewController.h"
#import "Connectivity.h"
#import "MBProgressHUD.h"
#import "ShowResultsTableViewController.h"
#import "NotificationManager.h"

typedef enum
{
    RepTypeRepresentative,
    RepTypeSenator
}RepType;

#define whoismyrepresentativeURL(method, zip) ([NSString stringWithFormat: @"http://whoismyrepresentative.com/%@.php?zip=%@&output=json", method, zip])

static NSString * const getall_mems = @"getall_mems";
static NSString * const getall_reps_byname = @"getall_reps_byname";
static NSString * const getall_reps_bystate = @"getall_reps_bystate";
static NSString * const getall_sens_byname = @"getall_sens_byname";
static NSString * const getall_sens_bystate = @"getall_sens_bystate";

@interface SearchByViewController ()

@property (nonatomic, strong) NSDictionary *resultsData;

@property (strong, nonatomic) MBProgressHUD *hud;

@property (strong, nonatomic) IBOutlet UISegmentedControl *repSelectionSegmentedControl;
@end

@implementation SearchByViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)pressedSearchType:(id)sender
{
    UIButton *btn = (UIButton *)sender;
    
    if ([btn.titleLabel.text isEqualToString:@"Zip"])
    {
        [self getResultsForURL:whoismyrepresentativeURL(getall_mems,@"31023")];
    }
    else if ([btn.titleLabel.text isEqualToString:@"State"])
    {
        [self getResultsForURL:whoismyrepresentativeURL(getall_mems,@"84606")];
    }
    else if ([btn.titleLabel.text isEqualToString:@"Name"])
    {
        [self getResultsForURL:whoismyrepresentativeURL(getall_mems,@"84606")];
    }
    else if ([btn.titleLabel.text isEqualToString:@"Automatic"])
    {
        [self getResultsForURL:whoismyrepresentativeURL(getall_mems,@"84606")];
    }
}

- (void)getResultsForURL:(NSString *)URLString
{
    
    if ([Connectivity connected])
    {
        [self setupHUD];
        
        NSURL *url = [NSURL URLWithString:URLString];
        
        NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if ([data length] > 0 && error == nil)
             {
                 NSError *JSONerror = nil;
                 NSDictionary *jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&JSONerror];
                 
                 if (JSONerror != nil)
                 {
                     NSLog(@"Error parsing JSON.");
                     NSLog(@"error: %@", JSONerror);

                     [self performSelectorOnMainThread:@selector(failedLoadingResults) withObject:nil waitUntilDone:YES];
                 }
                 else
                 {
                     NSLog(@"JSON Response: %@", jsonResponse);
                     self.resultsData = jsonResponse;
                     [self performSelectorOnMainThread:@selector(finishedLoadingResults) withObject:nil waitUntilDone:YES];
                 }
             }
             else if ([data length] == 0 && error == nil)
             {
                 NSLog(@"no data downloaded");
                 [self performSelectorOnMainThread:@selector(failedLoadingResults) withObject:nil waitUntilDone:YES];
             }
             else if (error != nil && error.code == NSURLErrorTimedOut)
             {
                 NSLog(@"URL request timed out");
                 [self performSelectorOnMainThread:@selector(failedLoadingResults) withObject:nil waitUntilDone:YES];
             }
         }];
    }
}

- (void)failedLoadingResults
{
    [self.hud hide:YES];
    self.hud = nil;
    [NotificationManager alertWithMessage:@"Error Getting Data"];
}

- (void)finishedLoadingResults
{
    [self.hud hide:YES];
    [self performSegueWithIdentifier:@"ShowResultsSegueway" sender:self];
}

- (void)setupHUD
{
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = @"Loading";
    [self.view addSubview:self.hud];
    [self.hud show:YES];
}

#pragma mark - Segueway Delegate Methods

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowResultsSegueway"])
    {
        UINavigationController *nc = (UINavigationController *)segue.destinationViewController;
        
        ShowResultsTableViewController *vc = (ShowResultsTableViewController *)[[nc viewControllers] objectAtIndex:0];
        vc.resultsDict = self.resultsData;
    }
    if ([segue.identifier isEqualToString:@"SearchByZipSegueway"])
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(receiveNotificationSearchByZipSegueway:)
                                                     name:@"SearchByZip"
                                                   object:nil];
    }
}

- (void)receiveNotificationSearchByZipSegueway:(NSNotification *) notification
{
    [self getResultsForURL:whoismyrepresentativeURL(getall_mems,[notification.userInfo objectForKey:@"zipCode"])];
}

@end
