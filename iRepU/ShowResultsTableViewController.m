//
//  ShowResultsTableViewController.m
//  iRepU
//
//  Created by Jaime Moises Gutierrez on 3/30/15.
//  Copyright (c) 2015 JMGL. All rights reserved.
//

#import "ShowResultsTableViewController.h"
#import "ResultsTableViewCell.h"
#import "ViewProfileViewController.h"

@interface ShowResultsTableViewController ()

@property (nonatomic, copy) NSArray *resultsArray;

@end

@implementation ShowResultsTableViewController

static NSString * const reuseIdentifier = @"ResultsCell";


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
     self.clearsSelectionOnViewWillAppear = NO;
    
    self.resultsArray = [self.resultsDict objectForKey:@"results"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.resultsArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 74;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ResultsTableViewCell *cell = (ResultsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    /*
     district = 3;
     link = "http://chaffetz.house.gov";
     name = "Jason Chaffetz";
     office = "2464 Rayburn House Office Building";
     party = R;
     phone = "202-225-7751";
     state = UT;
     */
    
    NSDictionary *repData =  [self.resultsArray objectAtIndex:indexPath.row];
    
    cell.nameLabel.text = [repData objectForKey:@"name"];
    cell.districtLabel.text = [NSString stringWithFormat:@"District: %@",[repData objectForKey:@"district"]];
    cell.stateLabel.text = [repData objectForKey:@"state"];
    cell.partyLabel.text = [repData objectForKey:@"party"];
    
    return cell;
}


//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    [self performSegueWithIdentifier:@"ShowProfileSegueway" sender:self];
//}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ShowProfileSegueway"])
    {
        ViewProfileViewController *vc = (ViewProfileViewController *)segue.destinationViewController;
        vc.profileDict = [self.resultsArray objectAtIndex:[self.tableView indexPathForSelectedRow].row];
    }
}

- (IBAction)pressedBackButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}


@end
